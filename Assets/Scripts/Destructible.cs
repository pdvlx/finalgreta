using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour {

	public GameObject destroyedVersion;
	public ParticleSystem toonEffect;
	private ParticleSystem shineEffect;
	private Rigidbody _rb;
	private bool _clickedFire2 , _inCoroutine;
	void OnMouseDown ()
	{
		if (!GetComponent<MeshRenderer>().enabled)
		{
			
			GetComponent<MeshRenderer>().enabled = true;
			if (_rb != null)
			{
				_rb.interpolation = RigidbodyInterpolation.Extrapolate;
				_rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
			}
			
		}
		else if (_clickedFire2)
		{
			if (_rb!= null)
			{
				_rb.interpolation = RigidbodyInterpolation.None;
				_rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
			}
			
			Instantiate(destroyedVersion, transform.position, transform.rotation);
			this.GetComponent<MeshRenderer>().enabled = false;
			_clickedFire2 = false;
			
			//=========
			
		}
	}

	private void Start()
	{
		if(GetComponent<Rigidbody>()!=null)
			_rb = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		if (Input.GetButton("Fire2"))
		{
			print("SKILL ACTIVE");
			_clickedFire2 = true;
			
		}
		else if (Input.GetButtonUp("Fire2"))
		{
			_clickedFire2 = false;
		}
	
	}

}
