﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTrigger : MonoBehaviour
{
    public static bool openHole = false;

    public GameObject light;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        openHole = true;

    }

    private void OnTriggerExit(Collider other)
    {
        openHole = false;
        
    }
}
